#!/system/bin/sh

setprop log.tag.InputMethodManagerService D
setprop log.tag.DoubleApp V
setprop log.tag.UsageStatsService D
setprop log.tag.ZteSecurityImeHelp D
setprop log.tag.ImeFocusController D
setprop log.tag.InputMethodManager D
setprop log.tag.InputMethodService D
setprop log.tag.ViewRootImpl D
setprop log.tag.KeyCombinationManager D
setprop log.tag.MediaProvider D
setprop log.tag.RecentTasks D