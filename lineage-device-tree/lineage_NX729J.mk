#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from NX729J device
$(call inherit-product, device/nubia/NX729J/device.mk)

PRODUCT_DEVICE := NX729J
PRODUCT_NAME := lineage_NX729J
PRODUCT_BRAND := nubia
PRODUCT_MODEL := NX729J
PRODUCT_MANUFACTURER := nubia

PRODUCT_GMS_CLIENTID_BASE := android-zte

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="NX729J-user 13 TKQ1.221220.001 20231201.162326 release-keys"

BUILD_FINGERPRINT := nubia/NX729J/NX729J:13/TKQ1.221220.001/20231201.162326:user/release-keys
