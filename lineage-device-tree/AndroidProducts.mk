#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_NX729J.mk

COMMON_LUNCH_CHOICES := \
    lineage_NX729J-user \
    lineage_NX729J-userdebug \
    lineage_NX729J-eng
