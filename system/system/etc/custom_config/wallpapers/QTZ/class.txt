<Optimus Prime>
<main>
default_wallpaper_rm13op1
<secondary>
default_wallpaper_rm13op2
default_wallpaper_rm13op3
default_wallpaper_rm13op4
default_wallpaper_rm13op5
default_wallpaper_rm13op6
default_wallpaper_rm13op7
default_wallpaper_rm13op8
default_wallpaper_rm13op9
default_wallpaper_rm13op10
default_wallpaper_rm13op11

<REDMAGIC Warrior>
<main>
default_wallpaper_rm13a1
<secondary>
default_wallpaper_rm13a2
default_wallpaper_rm13a3
default_wallpaper_rm12b4

<Metaverse>
<main>
default_wallpaper_rm13b1
<secondary>
default_wallpaper_rm13b2
default_wallpaper_rm13b3
default_wallpaper_rm12b1
default_wallpaper_rm12b2
default_wallpaper_rm12b3


<Fast and Furious>
<original>
default_wallpaper_rm13c1
default_wallpaper_rm13c2
default_wallpaper_rm13c3
default_wallpaper_rm13c4

<Space City>
<main>
default_wallpaper_rm13d1
<secondary>
default_wallpaper_rm13d2
default_wallpaper_rm13d3
default_wallpaper_rm13d4
default_wallpaper_rm13d5

<Metal Marines>
<main>
default_wallpaper_rm13e1
<secondary>
com.zte.livewallpaper.LiveWallpaper_rm12e1_Service
com.zte.livewallpaper.LiveWallpaper_rm12e2_Service
com.zte.livewallpaper.LiveWallpaper_rm12e3_Service
com.zte.livewallpaper.LiveWallpaper_rm12e4_Service

<Shadow Flow>
<original>
default_wallpaper_rm13f1
default_wallpaper_rm13f2
default_wallpaper_rm13f3

<Planets>
<original>
com.zte.livewallpaper.LiveWallpaper_m13k1_Service
com.zte.livewallpaper.LiveWallpaper_m13k2_Service
com.zte.livewallpaper.LiveWallpaper_m13k3_Service

<Sky Eye>
<main>
default_wallpaper_m13u3
<secondary>
default_wallpaper_m13u2
default_wallpaper_m13u4
default_wallpaper_m13u1
default_wallpaper_m12u2

<Simple>
<original>
default_wallpaper_m13b1
default_wallpaper_m13b2
default_wallpaper_m13b3